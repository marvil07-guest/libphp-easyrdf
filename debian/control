Source: libphp-easyrdf
Priority: optional
Maintainer: Marco Villegas <marco@marvil07.net>
Build-Depends:
 debhelper-compat (= 12),
 pkg-php-tools (>= 1.7~),
Build-Depends-Indep:
 composer,
Standards-Version: 4.4.0
Section: php
Homepage: http://www.easyrdf.org/
Vcs-Browser: https://salsa.debian.org/marvil07-guest/libphp-easyrdf
Vcs-Git: https://salsa.debian.org/marvil07-guest/libphp-easyrdf.git

Package: libphp-easyrdf
Architecture: all
Depends:
 php-mbstring,
 ${misc:Depends},
 ${phpcomposer:Debian-require},
Suggests:
 ${phpcomposer:Debian-suggest},
Provides:
 ${phpcomposer:Debian-provide},
Description: PHP library to consume and produce RDF
 EasyRdf is a PHP library designed to make it easy to consume and
 produce RDF.
 It was designed for use in mixed teams of experienced and inexperienced
 RDF developers.
 It is written in Object Oriented PHP and has been tested extensively
 using PHPUnit.
 .
 After parsing EasyRdf builds up a graph of PHP objects that can then be
 walked around to get the data to be placed on the page.
 Dump methods are available to inspect what data is available during
 development.
 .
 Data is typically loaded into a EasyRdf_Graph object from source RDF
 documents, loaded from the web via HTTP.
 The EasyRdf_GraphStore class simplifies loading and saving data to a
 SPARQL 1.1 Graph Store.
 .
 SPARQL queries can be made over HTTP to a Triplestore using the
 EasyRdf_Sparql_Client class.
 SELECT and ASK queries will return an EasyRdf_Sparql_Result object and
 CONSTRUCT and DESCRIBE queries will return an EasyRdf_Graph object.
